# README #


Business Card Printing Frequently Asked Questions
https://ctrl.cy/print/business-cards/

Q: What size are business cards?
A: So how big are business cards? Here’s a quick rundown of standard business card size:

Business card size in inches: 3.5" x 2"
Business card size in cm/mm: 85 x 55 mm 
Business card size in Photoshop: 1050 x 600 pixels

We also offer square cards or  rounded corner cards, which are a little different than typical business card size. Also, when you’re designing or choosing a template, remember that business card dimensions may include a “bleed area,” which is extra space for images, patterns or design elements that extend beyond the cut edges – this helps prevent having white edges around your finished card.

Q: How thick is a business card?
A: Business card thickness is usually measured in “grammes per square metre” abbreviated as “gsm” The higher the number, the thicker the card. Standard business card thickness is 300 gsm, but weights can vary by paper type. For instance, our ultra thick business cards are actually 650 gsm, and our premium cards come in at 360 gsm. 

  Q: What do I do with old business cards?
A: If the information is still mostly correct, you can keep them as backups by crossing out the out-of-date info and writing in new text. If the cards are mostly inaccurate, we’d recommend recycling them. 

Q: How do I organise business cards?
A: We’d recommend using one of our business card holders, available in an assortment of different materials and styles. 

Q: How do I pick a business card layout?
A: Traditionally, business cards have a horizontal layout, but if you’re going for a different, modern feel, we have a lot of templates for vertical business cards, too. As far as specific design layouts, we have thousands you can quickly sort and browse by industry, shape, style and more. And if you’re wondering what information you need to include (and where it should go), check out some articles about the 10 golden rules for creating a business card and some tips when designing your card.

Q: If I add a special finish to my card (like embossed gloss or foil accent), will it be on both sides? 
A: The front of your card will have whatever texture or finish you selected. The back will be a smooth, matte surface – great for adding notes, appointments or other information.

https://ctrl.cy/print/business-cards/
Whatever your need, we’ve got you covered